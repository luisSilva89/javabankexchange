package org.academiadecodigo.javabank.application.operations;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.integer.IntegerInputScanner;
import org.academiadecodigo.javabank.domain.Customer;
import org.academiadecodigo.javabank.managers.AccountManager;

public class Withdrawal implements Operation {
    @Override
    public void execute(AccountManager accountManager, Customer customer, Prompt prompt) {

        if (accountManager.getAccountMap().size() == 0) {
            System.out.println("You have no accounts on this bank \nreturning...");
            return;
        }


        System.out.println("\nYour accounts:");

        int accountId = 1;
        for (int i = 0; i < accountManager.getAccountMap().size(); i++) {
            System.out.println("[" + accountId + "]: " + accountManager.getAccountType(i + 1) + "  [balance]: " + accountManager.getAccountMap().get(i + 1).getBalance() + "€'s");
            accountId++;
        }


        IntegerInputScanner accountType = new IntegerInputScanner();
        accountType.setMessage("\nAccount to withdraw: ");
        int account = prompt.getUserInput(accountType);


        IntegerInputScanner valueToWithdraw = new IntegerInputScanner();
        valueToWithdraw.setMessage("Enter the value: ");
        int value = prompt.getUserInput(valueToWithdraw);

        accountManager.withdraw(account, value);

    }
}
