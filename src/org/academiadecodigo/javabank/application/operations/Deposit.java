package org.academiadecodigo.javabank.application.operations;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.integer.IntegerInputScanner;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.javabank.domain.Customer;
import org.academiadecodigo.javabank.domain.account.Account;
import org.academiadecodigo.javabank.managers.AccountManager;

public class Deposit implements Operation {
    @Override
    public void execute(AccountManager accountManager, Customer customer, Prompt prompt) {


        if (accountManager.getAccountMap().size() == 0) {
            System.out.println("You have no accounts on this bank \n returning...");
            return;
        }

        System.out.println("\nYour accounts:");


        int accountId = 1;
        for (int i = 0; i < accountManager.getAccountMap().size(); i++) {
            System.out.println("[" + accountId + "]: " + accountManager.getAccountMap().get(i + 1));
            accountId++;
        }

        IntegerInputScanner accountType = new IntegerInputScanner();
        accountType.setMessage("\nAccount to deposit: ");
        int account = prompt.getUserInput(accountType);


        IntegerInputScanner valueToDeposit = new IntegerInputScanner();
        valueToDeposit.setMessage("Enter the value: ");
        int value = prompt.getUserInput(valueToDeposit);

        accountManager.deposit(account, value);
        System.out.println("Operation successful!");
        System.out.println("Current balance: " + accountManager.getAccountMap().get(account).getBalance() + "€'s");
    }
}
