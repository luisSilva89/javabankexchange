package org.academiadecodigo.javabank.application.operations;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.integer.IntegerInputScanner;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.javabank.domain.Customer;
import org.academiadecodigo.javabank.domain.account.AccountType;
import org.academiadecodigo.javabank.managers.AccountManager;

public class OpenAccount implements Operation {
    @Override
    public void execute(AccountManager accountManager, Customer customer, Prompt prompt) {

        MenuInputScanner menuInputScanner = new MenuInputScanner(new String[]{"Checking Account", "Saving Account"});
        menuInputScanner.setMessage("What type of account do you wish to open?");
        int option = prompt.getUserInput(menuInputScanner);


        //AccountType.values() -> returns an array with Nº indexes that store an instance of the Enum AccountType
        System.out.println("Account created with ID: " + customer.openAccount(AccountType.values()[option -1]));

    }
}
