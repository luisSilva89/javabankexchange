package org.academiadecodigo.javabank.application.operations;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.javabank.domain.Customer;
import org.academiadecodigo.javabank.managers.AccountManager;

public class Quit implements Operation {
    @Override
    public void execute(AccountManager accountManager, Customer customer, Prompt prompt) {

        System.exit(0);
    }
}
