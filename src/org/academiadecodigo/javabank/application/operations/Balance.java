package org.academiadecodigo.javabank.application.operations;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.integer.IntegerInputScanner;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.javabank.domain.Customer;
import org.academiadecodigo.javabank.managers.AccountManager;

public class Balance implements Operation {

    @Override
    public void execute(AccountManager accountManager, Customer customer, Prompt prompt) {

        boolean quit = false;

        if (accountManager.getAccountMap().size() == 0) {
            System.out.println("You have no accounts on this bank \n returning...");
            return;
        }

        while (!quit) {

            MenuInputScanner menuInputScanner = new MenuInputScanner(new String[]{"Account balance", "Global balance", "Quit"});
            menuInputScanner.setMessage("Choose an option below: ");
            int option = prompt.getUserInput(menuInputScanner);

            if (option == 1) {

                System.out.println("\nYour accounts:");

                int accountId = 1;
                for (int i = 0; i < accountManager.getAccountMap().size(); i++) {
                    System.out.println("[" + accountId + "]: " + accountManager.getAccountType(i + 1));
                    accountId++;
                }


                IntegerInputScanner accountToCheck = new IntegerInputScanner();
                accountToCheck.setMessage("Input your account ID: ");
                int account = prompt.getUserInput(accountToCheck);

                System.out.println("Your current balance is: " + customer.getBalance(account) + "€");

            } else if (option == 2) {

                System.out.println("Your total balance is: " + customer.getBalance() + "€");

            } else if (option == 3) {

                quit = true;

            } else {
                System.out.println("Incorrect command, try again. ");
            }
        }
    }
}
