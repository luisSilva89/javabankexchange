package org.academiadecodigo.javabank.application;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.integer.IntegerInputScanner;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.javabank.application.operations.*;
import org.academiadecodigo.javabank.domain.Bank;
import org.academiadecodigo.javabank.domain.Customer;
import org.academiadecodigo.javabank.managers.AccountManager;
import java.util.Map;
import java.util.TreeMap;

public class BankApplication {

    private Map<Integer, Operation> operations = new TreeMap<>();
    private Prompt prompt = new Prompt(System.in, System.out);


    public void start() {

        operations.put(1, new Balance());
        operations.put(2, new Deposit());
        operations.put(3, new Withdrawal());
        operations.put(4, new OpenAccount());
        operations.put(5, new Quit());


        AccountManager accountManager = new AccountManager();
        Customer customer = new Customer("Luis", 0);

        customer.setAccountManager(accountManager);
        Bank bank = new Bank(accountManager);

        bank.addCustomer(customer);

        login(accountManager, customer);
    }

    // TODO: 12/11/20 método de registro de usuário

    // TODO: 12/11/20 metodo de validação de usuário e login na sua conta utilizador 


    public void login(AccountManager accountManager, Customer customer) {

        IntegerInputScanner id = new IntegerInputScanner();
        id.setMessage("Please insert your ID: ");
        prompt.getUserInput(id);

        while (true) {
            MenuInputScanner menuInputScanner = new MenuInputScanner(new String[]{"View Balance", "Make Deposit", "Make Withdrawal", "Open Account", "Quit"});
            menuInputScanner.setMessage("Welcome to Java Bank Mr. " + customer.getName());
            int option = prompt.getUserInput(menuInputScanner);

            operations.get(option).execute(accountManager, customer, prompt);

        }

    }

}
