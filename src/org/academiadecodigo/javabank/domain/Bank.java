package org.academiadecodigo.javabank.domain;

import org.academiadecodigo.javabank.managers.AccountManager;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * The bank entity
 */
public class Bank {

    private int customerId = 0;

    private AccountManager accountManager;
    private Map<Integer, Customer> customers = new HashMap<>();

    /**
     * Creates a new instance of Bank and initializes it with the given account manager
     *
     * @param accountManager the account manager
     */
    public Bank(AccountManager accountManager) {
        this.accountManager = accountManager;
    }

    /**
     * Adds a new customer to the bank
     *
     * @param customer the new bank customer
     * @see Customer#setAccountManager(AccountManager)
     */
    public void addCustomer(Customer customer) {
        customers.put(customerId, customer);
        customer.setAccountManager(accountManager);
        customerId++;
    }

    /**
     * Gets the total balance of the bank
     *
     * @return the bank total balance
     */

    public double getBalance() {

        double balance = 0;
        int id = 0;

        for(int i = 0; i < customers.size(); i++) {
            double customerTotal = customers.get(id).getBalance();
            balance = balance + customerTotal;
            id++;
        }
        return balance;
    }

    public boolean validateCustomerId(int id) {
        return customers.containsKey(id);
    }

}
