package org.academiadecodigo.javabank.domain.account;

import org.academiadecodigo.javabank.managers.AccountManager;

import java.util.NoSuchElementException;

public class AccountFactory {


    private static int numberAccounts = 0;

    public static Account createAccount(AccountType accountType) {

        Account newAccount;
        numberAccounts++;

        if (accountType == AccountType.CHECKING) {
            newAccount = new CheckingAccount(numberAccounts);

        } else {
            newAccount = new SavingsAccount(numberAccounts);
        }


        return newAccount;

    }




}
