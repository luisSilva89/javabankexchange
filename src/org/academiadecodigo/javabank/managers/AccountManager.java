package org.academiadecodigo.javabank.managers;

import org.academiadecodigo.javabank.domain.account.Account;
import org.academiadecodigo.javabank.domain.account.AccountType;
import org.academiadecodigo.javabank.domain.account.CheckingAccount;
import org.academiadecodigo.javabank.domain.account.SavingsAccount;

import java.util.HashMap;
import java.util.Map;

/**
 * Responsible for account management
 */
public class AccountManager {


    private Map<Integer, Account> accountMap;

    /**
     * Creates a new {@code AccountManager}
     */
    public AccountManager() {
        this.accountMap = new HashMap<>();
    }

    /**
     * Creates a new {@link Account}
     *
     * @param account the account type
     * @return the new account
     */

    // TODO: 11/11/20 Implementar Factory Method
    public void addAccount(Account account) {

      accountMap.put(account.getId(), account);
    }

    /**
     * Perform an {@link Account} deposit if possible
     *
     * @param id     the id of the account
     * @param amount the amount to deposit
     */
    public void deposit(int id, double amount) {
        accountMap.get(id).credit(amount);
    }

    /**
     * Perform an {@link Account} withdrawal if possible
     *
     * @param id     the id of the account
     * @param amount the amount to withdraw
     */
    public void withdraw(int id, double amount) {

        Account account = accountMap.get(id);


        if (!account.canWithdraw(amount)) {
            System.out.println("\nNot enough €'s available.");
            return;
        }

        accountMap.get(id).debit(amount);
        System.out.println("\nOperation successful");
    }

    /**
     * Performs a transfer between two {@link Account} if possible
     *
     * @param srcId  the source account id
     * @param dstId  the destination account id
     * @param amount the amount to transfer
     */
    public void transfer(int srcId, int dstId, double amount) {

        Account srcAccount = accountMap.get(srcId);
        Account dstAccount = accountMap.get(dstId);

        // make sure transaction can be performed
        if (srcAccount.canDebit(amount) && dstAccount.canCredit(amount)) {
            srcAccount.debit(amount);
            dstAccount.credit(amount);
        }
    }

    public Account getAccountType(int id) {
        return accountMap.get(id);
    }

    public Map<Integer, Account> getAccountMap() {
        return accountMap;
    }
}
